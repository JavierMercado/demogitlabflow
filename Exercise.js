const number1 = 5;
const number2 = 3;

function Sum(number1, number2) {
  const sum = number1 + number2;
  return 'The sum of ' + number1 + ' and ' + number2 + ' is: ' + sum;
}

console.log(Sum(number1, number2));
